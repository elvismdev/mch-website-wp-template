<?php
// Template Name: Property Listing
get_header();
?>

<div class="main-content">
	<div class="section">

		<?php
		if(is_page()) {

			$community = array(
				'post_type' => 'community',
				'posts_per_page'=> 3,
				'meta_key'   => 'wpcf-featured-community'
				);

			$community_query = new WP_Query($community);

			?>

			<div class="row communities-row">

				<h2 class="title">FEATURED COMMUNITIES</h2>

				<div class="large-12 columns">

					<ul class="large-block-grid-3 medium-block-grid-2">
						<?php while ( $community_query->have_posts() ) : $community_query->the_post(); ?>
							<li><?php nt_property_card($community_query->ID); ?></li>
						<?php endwhile; ?>
					</ul>

					<div class="vspace"></div>
				</div>

			</div>

			<?php wp_reset_postdata(); } ?>

			<?php
			$property = array(
				'post_type' => 'property',
				'paged'=> $paged
				);
			if(is_page()) {
				$wp_query = new WP_Query($property);
			}
			?>

			<div class="row">

				<h2 class="title residences-title">AVAILABLE RESIDENCES</h2>

				<div class="large-12 columns">
					<?php get_template_part('section/section', 'property-sort'); ?>

					<ul class="large-block-grid-3 medium-block-grid-2">
						<?php while ( have_posts() ) : the_post(); ?>
							<li><?php nt_property_card($post->ID); ?></li>
						<?php endwhile; ?>
					</ul>

					<div class="vspace"></div>
					<?php get_template_part('section/section', 'nav'); ?>
				</div>

			</div>
		</div>
	</div><!-- .main-content -->

	<?php get_footer(); ?>