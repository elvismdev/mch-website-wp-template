<?php

add_action( 'wp_enqueue_scripts', 'enqueue_custom_scripts' );

function enqueue_custom_scripts() {
	/* load Properticons Styles for theme. */
	wp_enqueue_style( 'properticons', get_stylesheet_directory_uri() . '/github-assets/properticons/css/properticons.css' );
	/* load Fontawesome Styles for theme. */
	wp_enqueue_style( 'fortawesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', array(), '4.4.0' );

	// Grayscale Filter Fix for IE on About Us page only
	if ( is_page( 2306 ) ) {
		wp_enqueue_style( 'grayscale', get_stylesheet_directory_uri() . '/grayscale-ie/css/grayscale.css', array(), null );
		wp_enqueue_script( 'modernizr-custom', get_stylesheet_directory_uri() . '/grayscale-ie/js/modernizr.custom.js', array(), null, true );
		wp_enqueue_script( 'imagesloaded', get_stylesheet_directory_uri() . '/grayscale-ie/js/imagesloaded.pkgd.min.js', array( 'modernizr-custom' ), null, true );
		wp_enqueue_script( 'grayscale-js', get_stylesheet_directory_uri() . '/grayscale-ie/js/grayscale.js', array( 'modernizr-custom' ), null, true );
		wp_enqueue_script( 'grayscale-functions', get_stylesheet_directory_uri() . '/grayscale-ie/js/functions.js', array( 'modernizr-custom' ), null, true );
	}
}

function close_realtor_form_after_sent_ok() { ?>
<script>
	function _cf7_2530_on_sent_ok_() {
		setTimeout(function () {
			jQuery('#popmake-2531').popmake('close');
    }, 3000); // 2 seconds
	}
</script><?php
}
add_action( 'wp_head', 'close_realtor_form_after_sent_ok', 1000 );

function fix_header_menu_position() {
	if( is_singular( array( 'property', 'community', 'agent' ) ) ): ?>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('#undefined-sticky-wrapper').css("position","inherit");
		});
	</script>
	<script>jQuery.noConflict();</script>
<?php endif;
}
add_action( 'wp_footer', 'fix_header_menu_position', 1000 );