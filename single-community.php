<?php get_header(); ?>

<div class="main-content">
	<?php get_template_part('section/section', 'page-title'); ?>
	<div class="section section-blog">
		<div class="row">
			<div class="large-8 columns">
				<?php while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="article-head community-img">
							<?php
							if( nt_get_option('blog', 'single_featured_img', 'on') == 'on' )
								if( get_post_thumbnail_id() ) {
									$thumbnail = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
									$thumbnail_meta = wp_get_attachment_metadata(get_post_thumbnail_id());
									echo "<img class='feature-image' src='".$thumbnail[0]."' alt='".$thumbnail_meta['image_meta']['title']."' />";
								}
								?>
							</div>
							<?php the_content(); ?>

							<div class="wpb_tabs wpb_content_element" data-interval="0">
								<div class="wpb_wrapper wpb_tour_tabs_wrapper ui-tabs vc_clearfix">
									<ul class="wpb_tabs_nav ui-tabs-nav vc_clearfix">
										<?php
// Find connected pages
										$connected = new WP_Query( array(
											'connected_type' => 'homes_to_communities',
											'connected_items' => get_queried_object(),
											'nopaging' => true,
											) );
											?>

											<?php
// Display connected Models tabs
											if ( $connected->have_posts() ) :
												?>
											<li class="active">
												<a href="#tab-models">Models</a>
											</li>
										<?php endif; ?>
										<li class="">
											<a href="#tab-directions">Directions</a>
										</li>
									</ul>
									<?php
// Display connected Models tabs containers
									if ( $connected->have_posts() ) :
										?>
									<div id="tab-models" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix active">
										<p><?php echo $connected->post_count; ?> found.</p>

										<div class="wpp_row_view wpp_property_view_result">
											<div class="all-properties">
												<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
													<div class="property_div property clearfix">

														<div class="wpp_overview_left_column" style="float:left; ">
															<div class="property_image">
																<a href="<?php the_permalink(); ?>" title="The Addison II at Timber Farms" class="property_overview_thumb property_overview_thumb_tiny_thumb fancybox_image thumbnail" rel="properties">
																	<?php the_post_thumbnail(); ?>
																</a>
															</div>
														</div>

														<div class="wpp_overview_right_column">

															<ul class="wpp_overview_data">
																<li class="property_title">
																	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
																</li>

																<?php $meta = get_post_meta( get_the_ID() ); ?>
																<li class="property_price_lch"><?php echo nt_currency( $meta['_meta_price'][0], ( isset( $meta['_meta_per'][0] ) ) ? $meta['_meta_per'][0] : '' ); ?></li>

																<?php $locations = get_the_terms( $post->ID , 'location' ); ?>

																<li class="property_location_lch">
																	<?php foreach ( $locations as $location ) { echo $location->name . ' | ' . substr( single_post_title( null, false ), 0, strpos( single_post_title( null, false ), ' at' ) ); } ?>
																</li>

																<li class="property_data_lch">
																	<i class="lt-icon flaticon-person1 big"></i> <?php echo $meta['_meta_bedroom'][0]; ?> &nbsp; <i class="lt-icon flaticon-shower5"></i> <?php echo $meta['_meta_bathroom'][0]; ?> &nbsp; <i class="lt-icon flaticon-display6"></i> <?php echo $meta['_meta_area'][0]; ?> ft<sup>2</sup>                                 </li>


																</ul>

															</div>
														</div>
														<?php $documents[] = get_post_meta( $post->ID, '_meta_attachment', true ); ?>
													<?php endwhile; ?>
												</div>
											</div>
										</div>
										<?php
// Prevent weirdness
										wp_reset_postdata();

										endif;
										?>
										<div id="tab-directions" class="wpb_tab ui-tabs-panel wpb_ui-tabs-hide vc_clearfix">
											<?php echo do_shortcode( '[MKGD dlocation="' . types_render_field( 'community-address', array( ) ) . '"]' ); ?>
										</div>
									</div>
								</div>

								<?php if( nt_get_option('blog', 'single_share', 'on') == 'on' ) get_template_part('section/section', 'share'); ?>

								<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'theme_front' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>

							</article>
						<?php endwhile; ?>

						<?php if(!post_password_required()) comments_template(); ?>

					</div>
					<aside class="large-3 columns">
						<div id="area-map" class="widget">
							<div class="widget-title">Area Map</div>
							<?php echo do_shortcode( '[google_map address="' . types_render_field( 'community-address', array( ) ) . '" icon="' .  get_stylesheet_directory_uri() . '/img/bp-icon.png" scrollwheel="false"]' ); ?>
						</div>

						<?php if( !empty($documents[0]) ): ?>

							<div id="documents" class="widget">
								<div class="widget-title">Documents</div>
								<ul>
									<?php foreach ($documents as $document): ?>
										<?php if(is_array($document)): ?>
											<?php foreach($document as $doc): $url = wp_get_attachment_url($doc['file']); ?>
												<li>
													<a target="_blank" href="<?php echo $url; ?>"><?php echo $doc['stack_title']; ?></a> <i class="fa fa-file-pdf-o fa-lg"></i>
												</li>
											<?php endforeach; ?>
										<?php endif; ?>
									<?php endforeach ?>
								</ul>
							</div>

						<?php endif; ?>

						<div id="sales-office" class="widget">
							<div class="widget-title">Sales Office</div>
							<span class="strong-text">SALES AGENT:</span>
							<br><br>
							<?php
							$arg = array(
								'post_type' => 'agent'
								);
							$agents = get_posts( $arg );
							?>
							<ul>
								<?php foreach ($agents as $agent): ?>
									<div class="agent-card clearfix">
										<div class="card-head clearfix">
											<img alt="<?php echo $agent->post_title; ?>" src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $agent->ID ) ); ?>" srcset="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $agent->ID ) ); ?>" class="avatar avatar-512 photo" height="512" width="512">
											<div class="title"><?php echo $agent->post_title; ?></div>
											<?php $agent_meta = get_post_meta( $agent->ID ); ?>
											<div class="sub">
												<ul class="inline-list">
													<li><?php echo $agent_meta['_meta_phone'][0]; ?></li><li><a href="mailto:<?php echo $agent_meta['_meta_email'][0]; ?>"><?php echo $agent_meta['_meta_email'][0]; ?></a></li>
												</ul>
											</div>
										</div>
									</div>
								<?php endforeach ?>
							</ul>
						</div>

						<?php dynamic_sidebar('blog'); ?>
					</aside>
				</div>
			</div>
		</div>

		<?php get_footer(); ?>