<footer class="footer-main <?php echo nt_get_option('footer', 'footer_element_style', 'element-dark') ?>">

	<?php if(nt_get_option('footer', 'footer_show', 'on') == 'on'): ?>
		<div class="footer-top">
			<div class="row">
				<?php if(nt_get_option('footer', 'pre_footer_columns', '4-cols') == '4-cols'): ?>
					<div class="large-3 medium-6 columns"><?php if ( dynamic_sidebar( 'footer-1' ) ); ?></div>
					<div class="large-3 medium-6 columns"><?php if ( dynamic_sidebar( 'footer-2' ) ); ?></div>
					<div class="large-3 medium-6 columns"><?php if ( dynamic_sidebar( 'footer-3' ) ); ?></div>
					<div class="large-3 medium-6 columns"><?php if ( dynamic_sidebar( 'footer-4' ) ); ?></div>
				<?php else: ?>
					<div class="large-4 medium-6 columns"><?php if ( dynamic_sidebar( 'footer-1' ) ); ?></div>
					<div class="large-4 medium-6 columns"><?php if ( dynamic_sidebar( 'footer-2' ) ); ?></div>
					<div class="large-4 medium-6 columns"><?php if ( dynamic_sidebar( 'footer-3' ) ); ?></div>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>

	<div class="footer-bottom">
		<div class="row">

			<div class="large-4 medium-6 columns">
				<div id="about-us-widget">
					<h2>OUR COMMITMENT</h2>
					<div class="about-us-text">
						<p>We pride ourselves in providing you first class quality and service at a value that is unsurpassed.</p>
						<p>4878 SW 74th Court | Miami, FL 33155</p>
						<p><span class="phone-number">1-786-458-1805</span></p>
					</div>
					<div class="copyright-text"><?php echo nt_get_option('footer', 'footer_text', 'Copyright &copy; '.date("Y").' '.get_bloginfo('name')); ?></div>
				</div>
			</div>
			<div class="large-4 medium-6 columns">
				<div id="instagram-feed-widget">
					<?php
					the_widget( 'null_instagram_widget', array(
						'title' => 'INSTAGRAM STREAM',
						'username' => 'mountaincovehomes',
						'number' => 8,
						'target' => '_self',
						'link' => 'Follow Us'
						)
					);
					?>
				</div>
			</div>
			<div class="large-4 medium-6 columns">
				<ul class="social-list">
					<?php if(is_array(nt_get_option('footer', 'social_items'))) foreach( nt_get_option('footer', 'social_items') as $link ): ?>
						<li><a href="<?php echo esc_url($link['stack_title']); ?>"><?php echo do_shortcode('[nt_icon id="'.$link['icon'].'"]'); ?></a></li>
					<?php endforeach; ?>
				</ul>

				<div class="bt-align-center">
					<a id="realtor-signup-form" href="#" target="_self" class="lt-button  secondary large i-right">REALTOR SIGNUP</a>
				</div>
			</div>

		</div>
	</div>

</footer>

<div class="mobile-menu">
	<nav>
		<?php if(nt_get_option('header', 'header_type') == 'logo-center') {
			wp_nav_menu( array( 'theme_location' => 'primary-left', 'container' => false, 'container_class' => false, 'menu_id' => false, 'fallback_cb' => '', 'depth' => 0  ) );
			wp_nav_menu( array( 'theme_location' => 'primary-right', 'container' => false, 'container_class' => false, 'menu_id' => false, 'fallback_cb' => '', 'depth' => 0  ) );
		} else {
			wp_nav_menu( array( 'theme_location' => 'primary', 'container' => false, 'container_class' => false, 'menu_id' => false, 'fallback_cb' => '', 'depth' => 0  ) );
		}
		?>
	</nav>

	<?php if(nt_get_option('header', 'show_topbar', 'on') == 'on'): ?>
		<?php wp_nav_menu( array( 'theme_location' => 'top-right', 'container' => 'nav', 'container_class' => false, 'menu_id' => false, 'fallback_cb' => '', 'depth' => 1  ) ); ?>

		<?php if(nt_get_option('header', 'show_login', 'on') == 'on'): ?>
			<nav><?php get_template_part('section/section', 'user-menu'); ?></nav>
			<?php get_template_part('section/section', 'login-register'); ?>
		<?php endif; ?>
	<?php endif; ?>
</div>

</div></div><!-- .layout-wrap -->

<div class="modal-mask">
	<div class="modal login-modal">
		<?php get_template_part('section/section', 'login-register'); ?>
		<i class="flaticon-cross97 close-bt"></i>
	</div>
</div>

<?php
global $nt_site_message;
if($nt_site_message): ?>
<div class="message-mask">
	<div class="inner">
		<p><?php echo esc_html($nt_site_message); ?></p>
		<i class="flaticon-check52"></i>
	</div>
</div>
<?php endif; ?>

<?php wp_footer(); ?>

</body>
</html>